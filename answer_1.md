# Agile development experience

## RTFM

Today, while working on some shell scripting to reorganize files, I couldn't get
*sed* (the Linux CLI tool) to work the way I wanted. As is my habit, I referred
to the tool's manual using the *man* command. After consulting the manual, a
simple search provided the flag I needed to adjust my script and achieve the
desired outcome.

## LMGTFY

Yesterday, during the development of a frontend application integrated with the
blockchain, I faced the challenge of determining the transaction fee without
executing the transaction itself. Utilizing a combination of Google search and
reference to the Ethers.js documentation, I quickly identified a suitable
function within the library tailored specifically for this purpose.

# Tech knowledge

## OS

I primarily use Fedora Linux as my daily driver and development environment,
although I feel totally comfortable using Windows as development environment as
I did in a previous job. Also I wouldn't have any problem working with other
Linux distro.

## Languages

- Go: I built the complete backend of a web quality assurance system which is
  used every day by some local companies and their employees.
  used everyday by some local companies and their employees.
- SQL: I have experience optimizing complex queries, managing database
  migrations and rollbacks, normalizing and denormalizing databases, and quickly
  exploring database schemas to have a better understanding of the software at
  hand.
- Python: I used it to automate some tasks while working for Perficient and to
  build the machine learning for my university thesis project. Also, I have
  taken some Django courses.
- Java: I worked as a backend engineer developing new features and maintaning
  the supply chain, delivery and ecommerce services for a national food company.
