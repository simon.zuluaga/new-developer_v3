"""
Tests for the solver
"""

import unittest
from main import two_sum

class TestMain(unittest.TestCase):
    """
    Tests for main.two_sum
    """
    def test_two_sum_ok(self):
        """Successful test case"""
        nums = [2,3,6,7]
        target = 9
        expected = [2, 7]
        self.assertEqual(two_sum(nums, target), expected)

    def test_two_sum_nil(self):
        """Not found test case"""
        nums = [1,3,3,7]
        target = 9
        expected = []
        self.assertEqual(two_sum(nums, target), expected)

if __name__ == '__main__':
    unittest.main()
