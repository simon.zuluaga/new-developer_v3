"""
Solution implementation for the question #4 of RebajaTusCuentas new-developer
challenge.
"""

import sys

def two_sum(nums: list[int], target: int) -> list[int]:
    """
    Find a pair of numbers in 'nums' that sum up to 'target'.

    Args:
        nums (list[int]): List of integers organized in ascending order.
        target (int): Target sum for matching pair of nums.

    Returns:
        list[int]: Pair of numbers in 'nums' that sum up to 'target' or
        empty list if not found.
    """
    left_index, right_index = 0, len(nums) - 1

    while left_index < right_index:
        current_sum = nums[left_index] + nums[right_index]

        if current_sum == target:
            return [nums[left_index], nums[right_index]]
        if current_sum < target:
            left_index += 1
        else:
            right_index -= 1

    return []

def solve() -> None:
    """
    Wrapper for using two_sum() with stdin input.
    """
    nums = [int(x) for x in sys.stdin.readline().strip().split()]
    target = int(sys.stdin.readline().strip())
    print(two_sum(nums, target))

if __name__ == "__main__":
    solve()
