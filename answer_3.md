# Portfolio

## Papyrus

I am the co-author of Papyrus, a web-based quality assurance system that helps
companies organize their operations and documents with its web-based file system
focused on project management and accountability tracking. Additionally, it
provides metrics for task completion and date compliance.

The software is under a proprietary license, but I have complete and rightful
access to it in case you want to see more source code.

Here is the code for saving multiple tasks to the PostrgreSQL database:

```go
func (r *postgresTaskRepository) StoreMultiple(ctx context.Context, tasks []domain.Task) (err error) {
	tx, err := r.Conn.Begin()
	if err != nil {
		r.log.Err("IN [StoreMultiple] failed to begin transaction ->", err)
	}
	defer tx.Rollback()

	query := `INSERT INTO task (
			name,
			procedure,
			date_creation,
			term,
			state,
			dir,
			creator_user,
			recv_user,
			chk,
			plan
		)
		VALUES (
			$1,
			$2,
			$3,
			$4,
			$5,
			$6,
			$7,
			$8,
			$9,
			$10
		)
		RETURNING uuid`

	stmt, err := r.Conn.PrepareContext(ctx, query)
	if err != nil {
		r.log.Err("IN [StoreMultiple] failed to prepare context ->", err)
		return
	}
	defer stmt.Close()

	for _, t := range tasks {
		_, err = stmt.ExecContext(
			ctx,
			t.Name,
			t.Procedure,
			t.DateCreation,
			t.Term,
			1,
			t.Dir,
			t.CreatorUser,
			t.RecvUser,
			false,
			t.Plan,
		)

		if err != nil {
			r.log.Err("IN [StoreMultiple] failed to store task ->", err)
			return
		}
	}

	err = tx.Commit()
	if err != nil {
		r.log.Err("IN [StoreMultiple] failed to commit changes -> ", err)
		return err
	}

	return
}
```

# League of Legengs API

[**Repo:**](https://www.github.com/sicozz/lolapi)

This is an API I built during my internship at Perficient Latin America. It is a
RESTful API that utilizes data from the Riot Games API to calculate various
statistics.

This project helped me familiarize myself with technologies such as:

- Nodejs
- Express
- SQL
- MongoDB
- Redis
- Docker
- OAuth
